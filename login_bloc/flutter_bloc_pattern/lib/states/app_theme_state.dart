import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';

class AppThemeState extends Equatable{
  final ThemeData themeData;
  AppThemeState({this.themeData});
  @override
  List<Object> get props => [themeData];

}
class AppThemeInitState extends AppThemeState {}

class AppThemeLightState extends AppThemeState {}
class AppThemeDarkState extends AppThemeState {}