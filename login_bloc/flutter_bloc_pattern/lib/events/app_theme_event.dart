import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';

class AppThemeEvent extends Equatable{
  final ThemeData themeData;
  const AppThemeEvent({this.themeData});
  @override
  List<Object> get props => [themeData];
}

class StartThemeEvent extends AppThemeEvent {}
class ThemeChangedEvent extends AppThemeEvent{
  final bool isTheme;
  ThemeChangedEvent({this.isTheme});
}
