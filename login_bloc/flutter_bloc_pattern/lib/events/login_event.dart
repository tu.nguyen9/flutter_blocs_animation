import 'package:equatable/equatable.dart';

class LoginEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class StartEvent extends LoginEvent {}

class LoginButtonPress extends LoginEvent {
  final String email;
  final String password;

  LoginButtonPress({this.email, this.password});
}
