import 'package:dio/dio.dart';
import 'package:flutter_bloc_pattern/contents/app_contant.dart';
import 'package:flutter_bloc_pattern/models/login_model/body_login.dart';
import 'package:flutter_bloc_pattern/models/login_model/login_model.dart';
import 'package:logger/logger.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:retrofit/retrofit.dart';

part 'login_repository.g.dart';

@RestApi(baseUrl: AppContent.apiUrl)
abstract class LoginRepository {
  factory LoginRepository(Dio dio, {String baseUrl}) = _LoginRepository;
  static LoginRepository create() {
    final dio = Dio();
    dio.interceptors.add(PrettyDioLogger());
    return LoginRepository(dio);
  }

  @POST("login")
  Future<LoginModel> getLogin(@Header("apikey") String apiKey, @Body() BodyLogin bodyLogin);
}
