import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_pattern/blocs/app_theme_bloc.dart';
import 'package:flutter_bloc_pattern/blocs/login_bloc.dart';
import 'package:flutter_bloc_pattern/repository/login/login_repository.dart';
import 'package:flutter_bloc_pattern/states/app_theme_state.dart';
import 'package:flutter_bloc_pattern/states/login_state.dart';
import 'package:flutter_bloc_pattern/ui/home/setting/setting_theme/set_theme_page.dart';
import 'package:flutter_bloc_pattern/ui/page/login/login_page.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';

import 'contents/string_prefs.dart';
import 'events/app_theme_event.dart';

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);
  // static void setLocale(BuildContext context, Locale newLocale) {
  //   _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
  //   state.setLocale(newLocale);
  //}
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  //Locale _locale;

  @override
  void initState() {
    // if(SharedPreferencesUtil.getString(changeTheme) == "dark"){
    //   BlocProvider.of<AppThemeBloc>(context).add(
    //       AppThemeEvent(
    //           themeData: AppThemeChangeProvider
    //               .themeDarkData));
    //   print(SharedPreferencesUtil.getString(changeTheme));
    // }
    // else{
    //   BlocProvider.of<AppThemeBloc>(context).add(
    //       AppThemeEvent(
    //           themeData: AppThemeChangeProvider
    //               .themeLightData));
    //   print(SharedPreferencesUtil.getString(changeTheme));
    // }
    super.initState();
  }

  @override
  void didChangeDependencies() {
    // getLocale().then((locale) {
    //   setState(() {
    //     this._locale = locale;
    //   });
    // });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
            create: (context) =>
                LoginBloc(LoginInitState(), LoginRepository.create())),
        BlocProvider<AppThemeBloc>(create: (context) => AppThemeBloc()),
      ],
      child:
          BlocBuilder<AppThemeBloc, AppThemeState>(builder: (context, state) {
        return MaterialApp(
          theme: state.themeData,
          debugShowCheckedModeBanner: false,
          home: LoginPage(),
        );
      }),
    );
  }
  // setLocale(Locale locale) {
  //   setState(() {
  //     _locale = locale;
  //   });
  // }
}
