// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data_login.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DataLogin _$DataLoginFromJson(Map<String, dynamic> json) {
  return DataLogin(
    id: json['id'] as String,
    email: json['email'] as String,
    role: json['role'] as String,
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$DataLoginToJson(DataLogin instance) => <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'role': instance.role,
      'token': instance.token,
    };
