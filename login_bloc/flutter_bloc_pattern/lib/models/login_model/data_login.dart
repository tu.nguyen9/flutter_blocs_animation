import 'package:json_annotation/json_annotation.dart';

part 'data_login.g.dart';
@JsonSerializable()
class DataLogin{
  String id;
  String email;
  String role;
  String token;
  DataLogin({this.id, this.email, this.role, this.token});
  factory DataLogin.fromJson(Map<String, dynamic> json) => _$DataLoginFromJson(json);
  Map<String, dynamic> toJson() => _$DataLoginToJson(this);
}