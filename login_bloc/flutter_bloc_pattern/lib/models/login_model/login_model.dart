
import 'package:json_annotation/json_annotation.dart';

import 'data_login.dart';

part 'login_model.g.dart';
@JsonSerializable()
class LoginModel{

  bool status;
  DataLogin data;

  LoginModel({this.status, this.data});
  factory LoginModel.fromJson(Map<String, dynamic> json) => _$LoginModelFromJson(json);
  Map<String, dynamic> toJson() => _$LoginModelToJson(this);
}