import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc_pattern/app.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPreferencesUtil.getInstance();

  // final appDocumentDirectory =
  // await pathProvider.getApplicationDocumentsDirectory();
  // Hive.init(appDocumentDirectory.path);
  //
  // final settings = await Hive.openBox('settings');
  // bool isThemeLight = settings.get('isThemeLight') ?? false;
 runApp( AppStart());
}
class AppStart extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MyApp(
    );
  }
}