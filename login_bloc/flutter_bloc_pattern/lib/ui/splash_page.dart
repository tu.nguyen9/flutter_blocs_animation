import 'dart:async';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  AnimationController controller;
  CurvedAnimation curve;
  //LocationData _currentPosition; // create vali luu vi tri, dia chi, hien thi ra ban do.
  String _address, _dateTime;
  //Location location = Location();
  //LatLng _initialCameraPosition = LatLng(0.5937, 0.9629);

  _delayNavigator() async {
    await Future.delayed(Duration(seconds: 3), () {
      // Navigator.of(context).push(
      //     MaterialPageRoute(builder: (context) => InputOTPPage()));
      Navigator.of(context)
          .pushNamedAndRemoveUntil("onSet()", (Route<dynamic> route) => false);
      //Navigator.of(context).pushNamed(onSet());
      //Navigator.pushReplacementNamed(context, onSet());
    });
  }

  _animationCurved() {
    controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );
    curve = CurvedAnimation(parent: controller, curve: Curves.easeIn);
    controller.forward();
  }
  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
  @override
  void initState() {
    _animationCurved();
    super.initState();
  }
  @override
  void didChangeDependencies() {
    getLocationPermission();
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    // final connectivityStatus = Provider.of<ConnectivityStatus>(context);
    // if (connectivityStatus == ConnectivityStatus.Cellular ||
    //     connectivityStatus == ConnectivityStatus.Wifi) {
      _delayNavigator();
      //providerApi();
      return Scaffold(
        backgroundColor: Colors.yellowAccent,
        body: Center(
          child: FadeTransition(
            opacity: curve,
            child: Image.asset(
              "assets/image/lauch/app_splash.png",
              height: 300,
              width: 300,
            ),
          ),
        ),
      );
    // } else {
    //   return  NetworkSensitive();
    // }
  }

  // String onSet() {
  //   if (AddSharedPreferencesBase.getString(StringsPrefs.UserId) == '') {
  //     return loginPage;
  //   }
  //   return homePage;
  // }

  // providerApi() async {
  //   final api = Provider.of<ApiServer>(context, listen: false);
  //   api.getTasks().then((it) {
  //     AddSharedPreferencesBase.putString(StringsPrefs.ApiKey, it.data);
  //     print(it.data);
  //   }).catchError((onError) {
  //     print("run" + onError.toString());
  //   });
  //   return null;
  // }
  Future<void> getLocationPermission() async {
    // _currentPosition = await location.getLocation();
    // checkPermissionLocation.checkLocation().then((value){
    //   if(value == true){
    //     _initialCameraPosition =
    //         LatLng(_currentPosition.latitude, _currentPosition.longitude);
    //     location.onLocationChanged.listen((LocationData currentLocation) {
    //       print("${_currentPosition.latitude} : ${_currentPosition.longitude}");
    //       _currentPosition = currentLocation;
    //       _initialCameraPosition =
    //           LatLng(_currentPosition.latitude, _currentPosition.longitude);
    //       AddSharedPreferencesBase.putDouble(StringsPrefs.Latitude, _currentPosition.latitude);
    //       AddSharedPreferencesBase.putDouble(StringsPrefs.Longitude, _currentPosition.longitude);
    //       DateTime now = DateTime.now();
    //       _dateTime = DateFormat('EEE d MMM kk:mm:ss ').format(now);
    //       checkPermissionLocation.getAddress(_currentPosition.latitude, _currentPosition.longitude)
    //           .then((value) {
    //         List ls = value.first.addressLine.split(',');
    //         _address = "${ls[0]}";
    //
    //         AddSharedPreferencesBase.putString(StringsPrefs.AddressLocation, _address);
    //         print(AddSharedPreferencesBase.getString(StringsPrefs.AddressLocation));
    //       });
    //     });
    //   }
    //   else{
    //     checkPermissionLocation.showPermissionLocation(context);
    //   }
    // });
  }
}


