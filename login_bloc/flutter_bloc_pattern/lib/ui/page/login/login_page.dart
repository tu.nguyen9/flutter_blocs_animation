import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_pattern/blocs/login_bloc.dart';
import 'package:flutter_bloc_pattern/contents/string_prefs.dart';
import 'package:flutter_bloc_pattern/events/login_event.dart';
import 'package:flutter_bloc_pattern/states/login_state.dart';
import 'package:flutter_bloc_pattern/ui/home/setting/setting_theme/set_theme_page.dart';
import 'package:flutter_bloc_pattern/ui/widget/gradient_text.dart';
import 'package:flutter_bloc_pattern/ui/widget/raised_gradient_button_widget.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController _emailTEC = TextEditingController();
  TextEditingController _passwordTEC = TextEditingController();

  LoginBloc loginBloc;
  @override
  void initState() {
    loginBloc = BlocProvider.of<LoginBloc>(context);
    super.initState();
  }
  @override
  void dispose() {
    loginBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        body: BlocListener<LoginBloc, LoginState>(
          listener: (context, state){
            if(state is UserLoginSuccessState){
              print("user");
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=> SetThemePage()));
            }
            else if(state is AdminLoginSuccessState){
              print("admin");
            }else if(state is LoginLoadingState){
              return CircularProgressIndicator();
            }
            else{
              print("Login fail");
            }
          },
          child: SafeArea(
            top: true,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  GradientText(
                      Text(
                        "Login Application",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 40,
                        ),
                      ),
                      gradient: LinearGradient(colors: [
                        Colors.red,
                        Colors.pink,
                        Colors.purple,
                        Colors.deepPurple,
                        Colors.deepPurple,
                        Colors.indigo,
                        Colors.blue,
                        Colors.lightBlue,
                        Colors.cyan,
                        Colors.teal,
                        Colors.green,
                        Colors.lightGreen,
                        Colors.lime,
                        Colors.yellow,
                        Colors.amber,
                        Colors.orange,
                        Colors.deepOrange,
                      ])),
                  SizedBox(
                    height: width * .15,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: _emailEditText(),
                  ),
                  SizedBox(
                    height: width * 0.06,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: _passEditText(),
                  ),
                  SizedBox(
                    height: width * 0.01,
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                      child: StreamBuilder(
                        stream: loginBloc.submitValid,
                        builder: (context, snapshot) => RaisedGradientButton(
                          radius: 12,
                          gradient: LinearGradient(
                              end: Alignment.topRight,
                              begin: Alignment.bottomLeft,
                              colors: [Colors.blue, Colors.yellowAccent]),
                          child: Text(
                            "Login",
                            style: TextStyle(
                                color: Colors.white, fontWeight: FontWeight.bold),
                          ),
                          height: width * .15,
                          width: width,
                          function: snapshot.hasData
                              ? () {
                            print("onclick");
                            loginBloc.add(LoginButtonPress(email: _emailTEC.text, password: _passwordTEC.text));
                          }
                              : null,
                          colorBoxShadow: Colors.yellowAccent,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ));
  }

  Widget _emailEditText() => StreamBuilder(
      stream: loginBloc.emailS,
      builder: (context, snapshot) => TextField(
            controller: _emailTEC,
            textInputAction: TextInputAction.done,
            keyboardType: TextInputType.emailAddress,
            onChanged: loginBloc.emailChange,
            decoration: InputDecoration(
                errorText: snapshot.error,
                suffixIcon: Icon(Icons.design_services_outlined),
                hintText: "Enter Email",
                prefixIcon: Icon(Icons.email),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)))),
          ));
  Widget _passEditText() => StreamBuilder(
      stream: loginBloc.passWordS,
      builder: (context, snapshot) => TextField(
            //keyboardType: TextInputType.visiblePassword,
            controller: _passwordTEC,
            obscureText: true,
            onChanged: loginBloc.passwordChange,
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(
                errorText: snapshot.error,
                suffixIcon: Icon(Icons.design_services_outlined),
                hintText: "Enter Password",
                prefixIcon: Icon(Icons.email),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(12.0)))),
          ));
  final msg = BlocBuilder<LoginBloc, LoginState>(builder: (context, state){
    if(state is LoginErrorState){
      return Text(state.message);
    }else if(state is LoginLoadingState){
      return Center(child: CircularProgressIndicator(),);
    }else{
      return Container();
    }
  });
}
