import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_pattern/blocs/app_theme_bloc.dart';
import 'package:flutter_bloc_pattern/contents/string_prefs.dart';
import 'package:flutter_bloc_pattern/events/app_theme_event.dart';
import 'package:flutter_bloc_pattern/ui/widget/z_animated_toggle.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';
import 'package:provider/provider.dart';

class SetThemePage extends StatefulWidget {
  @override
  _SetThemePageState createState() => _SetThemePageState();
}

class _SetThemePageState extends State<SetThemePage>
    with SingleTickerProviderStateMixin {
  bool _isThemeSwitch;
  @override
  void initState() {
    _isThemeSwitch = false;
    super.initState();
  }
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    //final themeProvider = Provider.of<AppThemeChangeProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Set Theme Application"),
        leading: IconButton(icon: Icon(Icons.arrow_back_rounded),),
      ),
      body: SafeArea(
        top: true,
        child: Container(
          height: height,
          width: width,
          child: Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
            child: Text(!_isThemeSwitch ? "Light" : "Dark"),),
            Positioned(
              top: 150,
              left: 10, right: 10,
              child: Icon(
                Icons.settings_sharp,
                size: 60,
              ),
            ),
            Positioned(
              top: 250,
              // left: 0, right: 0,
              child: RaisedButton(
                onPressed: () {},
                child: Text(!_isThemeSwitch ? "Light" : "Dark"),
              ),
            ),
            Positioned(
                top: 300,
                // left: 0, right: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text("Change theme: "),
                    Switch(
                        value: _isThemeSwitch,
                        onChanged: (val) {
                          _isThemeSwitch = val;
                          setState(() {});
                          if (_isThemeSwitch) {
                            BlocProvider.of<AppThemeBloc>(context).add(
                                AppThemeEvent(
                                    themeData:
                                        AppThemeChangeProvider.themeDarkData));
                            SharedPreferencesUtil.putString(
                                changeTheme, "dark");
                            SharedPreferencesUtil.getString(changeTheme);
                            print(SharedPreferencesUtil.getString(changeTheme));
                          } else {
                            BlocProvider.of<AppThemeBloc>(context).add(
                                AppThemeEvent(
                                    themeData:
                                        AppThemeChangeProvider.themeLightData));
                            SharedPreferencesUtil.putString(
                                changeTheme, "light");
                            print(SharedPreferencesUtil.getString(changeTheme));
                          }
                        })
                  ],
                ))
          ]),
        ),
      ),
    );
  }

  Container buildDot({double width, double height, Color color}) {
    return Container(
      width: width,
      height: height,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        color: color,
      ),
    );
  }
}
