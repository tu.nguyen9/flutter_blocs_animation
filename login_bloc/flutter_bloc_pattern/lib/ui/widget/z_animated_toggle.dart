import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';
import 'package:provider/provider.dart';

class ZAnimatedToggle extends StatefulWidget {
  final List<String> values;
  final ValueChanged onToggleCallback;

  ZAnimatedToggle(
      {Key key, @required this.values, @required this.onToggleCallback})
      : super(key: key);

  @override
  _ZAnimatedToggleState createState() => _ZAnimatedToggleState();
}

class _ZAnimatedToggleState extends State<ZAnimatedToggle> {
  AppThemeChangeProvider appThemeChangeProvider;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //final themeProvider = Provider.of<AppThemeChangeProvider>(context);
    return Container(
      width: width * .7,
      height: width * .13,
      child: Stack(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              setState(() {
                widget.onToggleCallback(1);
              });
            },
            child: Container(
              width: width * .7,
              height: width * .13,
              decoration: ShapeDecoration(
                  //color: themeProvider.themeMode().toggleButtonColor,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width * .1))),
              child: Row(
                  children: List.generate(
                      widget.values.length,
                          (index) => Padding(
                        padding:
                        EdgeInsets.symmetric(horizontal: width * .1),
                        child: Text(
                          widget.values[index],
                          style: TextStyle(
                              fontSize: width * .05,
                              fontWeight: FontWeight.bold,
                              color: Colors.black87),
                        ),
                      ))),
            ),
          ),
          AnimatedAlign(
            alignment: Alignment.center,
            // alignment: themeProvider.isThemeLight
            //     ? Alignment.centerLeft
            //     : Alignment.centerRight,
            duration: Duration(milliseconds: 350),
            curve: Curves.ease,
            child: Container(
              alignment: Alignment.center,
              width: width * .35,
              height: width * .13,
              decoration: ShapeDecoration(
                  // color: themeProvider.themeMode().toggleButtonColor,
                  // shadows: themeProvider.themeMode().shadow,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(width * .1))),
              child: Text("",
                  // themeProvider.isThemeLight
                  //     ? widget.values[0]
                  //     : widget.values[1],
                  style: TextStyle(
                    fontSize: width * .045,
                    fontWeight: FontWeight.bold,
                  )),
            ),
          )
        ],
      ),
    );
  }
}
