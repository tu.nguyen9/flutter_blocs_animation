import 'package:flutter/cupertino.dart';

class GradientText extends StatelessWidget {
  final Text text;
  final Gradient gradient;
  const GradientText(this.text, {@required this.gradient,});
  @override
  Widget build(BuildContext context) {
    return ShaderMask(
      child: text,
        shaderCallback: (bound) => gradient
            .createShader(Rect.fromLTWH(0, 0, bound.width, bound.height)));
  }
}
