import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RaisedGradientButton extends StatelessWidget{
  final double radius;
  final Widget child;
  final Function function;
  final double width;
  final double height;
  final Gradient gradient;
  final Color colorBoxShadow;

  const RaisedGradientButton({this.radius, this.child, this.function, this.width,
      this.height, this.gradient,this.colorBoxShadow});

  @override
  Widget build(BuildContext context) {
    return Container(
      width:  width, height: height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        gradient: gradient, boxShadow: [
          BoxShadow(color: colorBoxShadow, offset: Offset(0.0, 1.5), blurRadius: 1.5),
      ]
      ),
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        elevation: 2.0,
        color: Colors.transparent,
        child: InkWell(
          onTap: function,
          child: Center(child: child,),
        ),
      ),
    );
  }
}