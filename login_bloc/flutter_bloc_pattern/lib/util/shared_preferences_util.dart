import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtil {
  static SharedPreferencesUtil _storageUtil;
  static SharedPreferences _preferences;

  static Future<SharedPreferencesUtil> getInstance() async {
    if (_storageUtil == null) {
      var secureStorage = SharedPreferencesUtil._();
      await secureStorage._init();
      _storageUtil = secureStorage;
    }
    return _storageUtil;
  }

  SharedPreferencesUtil._();
  Future _init() async {
    _preferences = await SharedPreferences.getInstance();
  }

  // put string
  static Future<bool> putString(String key, String value) {
    if (_preferences == null) return null;
    return _preferences.setString(key, value);
  }

  // get string
  static String getString(String key, {String defValue = ''}) {
    if (_preferences == null) return defValue;
    return _preferences.getString(key) ?? defValue;
  }

  // set double
  static Future<bool> putDouble(String key, double value) {
    if (_preferences == null) return null;
    return _preferences.setDouble(key, value);
  }

  // get double
  static double getDouble(String key, {double defValue = 0}) {
    if (_preferences == null) return defValue;
    return _preferences.getDouble(key) ?? defValue;
  }

  // clear string
  static Future<bool> clearString() {
    SharedPreferences prefs = _preferences;
    prefs.clear();
  }

  static Future<bool> commit() async => true;
}
