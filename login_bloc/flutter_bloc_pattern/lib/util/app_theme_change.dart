import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';

class AppThemeChangeProvider extends ChangeNotifier {
  bool isThemeLight = false;
  ThemeData _themeData;
  getCurrentStatusNavigationBarColor() {
    if (isThemeLight) {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarBrightness: Brightness.light,
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
          systemNavigationBarColor: Colors.white,
          systemNavigationBarIconBrightness: Brightness.dark));
    } else {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark,
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.light,
          systemNavigationBarColor: Colors.black38,
          systemNavigationBarIconBrightness: Brightness.dark));
    }
  }
  toggleThemeData() async {
    final setting = await Hive.openBox('settings');
    setting.put("isThemeLight", !isThemeLight);
    isThemeLight = !isThemeLight;
    getCurrentStatusNavigationBarColor();
    notifyListeners();
  }

  static final ThemeData themeLightData = ThemeData(
      scaffoldBackgroundColor: Colors.white,
      primaryColor: Colors.white,
      canvasColor: Colors.white,
      appBarTheme: AppBarTheme(
          color: Colors.white,
          iconTheme: IconThemeData(color: Colors.white)),
      iconTheme: IconThemeData(color: Colors.black54),
      textTheme: TextTheme(
          bodyText1: TextStyle(
            color: Colors.grey[800],
          ),
          bodyText2: TextStyle(color: Colors.grey[800]),
          headline4: TextStyle(color: Colors.grey[900])),
      unselectedWidgetColor: Colors.blue[400],
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.blueAccent, textTheme: ButtonTextTheme.primary),
      // visualDensity: VisualDensity.adaptivePlatformDensity,
      // primarySwatch: Colors.grey,
      // primaryColor: Colors.white,
       //brightness: Brightness.light,
      // backgroundColor: Colors.white,
      // scaffoldBackgroundColor: Colors.white
  );

  static final ThemeData themeDarkData = ThemeData(
      scaffoldBackgroundColor: Colors.grey[700],
      canvasColor: Colors.grey[800],
      toggleableActiveColor: Colors.grey[300],
      appBarTheme: AppBarTheme(
          color: Colors.grey[900],
          iconTheme: IconThemeData(color: Colors.grey[100])),
      iconTheme: IconThemeData(color: Colors.grey[300]),
      textTheme: TextTheme(
          bodyText1: TextStyle(
            color: Colors.grey[100],
          ),
          bodyText2: TextStyle(color: Colors.grey[100]),
          headline4: TextStyle(color: Colors.grey[50])),
      unselectedWidgetColor: Colors.grey[300],
      dividerColor: Colors.grey[500],
      buttonTheme: ButtonThemeData(
          buttonColor: Colors.grey[900], textTheme: ButtonTextTheme.primary),
      // visualDensity: VisualDensity.adaptivePlatformDensity,
      // primarySwatch: Colors.white54,
      // primaryColor: Colors.black38,
       //brightness: Brightness.dark,
      // backgroundColor: Colors.black38,
      // scaffoldBackgroundColor: Colors.black38

  );

  ThemeColor themeMode() {
    return ThemeColor(
        gradient: [
          if (isThemeLight) ...[Colors.pink, Colors.yellowAccent],
          if (!isThemeLight) ...[Colors.blue, Colors.lightBlue]
        ],
        textColor: isThemeLight ? Colors.black : Colors.white,
        toggleBackgroundColor: isThemeLight ? Colors.white : Colors.black87,
        toggleButtonColor: isThemeLight ? Colors.white38 : Colors.black87,
        shadow: [
          if (isThemeLight)
            BoxShadow(
                color: Colors.white70,
                spreadRadius: 5,
                blurRadius: 10,
                offset: Offset(0, 5.0)),
          if (!isThemeLight)
            BoxShadow(
                color: Colors.black38,
                spreadRadius: 5,
                blurRadius: 10,
                offset: Offset(0, 5.0))
        ]);
  }
  ThemeData get getTheme => _themeData;
}

class ThemeColor {
  List<Color> gradient;
  Color backgroundColor;
  Color toggleButtonColor;
  Color textColor;
  Color toggleBackgroundColor;
  List<BoxShadow> shadow;

  ThemeColor(
      {this.gradient,
      this.backgroundColor,
      this.toggleButtonColor,
      this.textColor,
      this.toggleBackgroundColor,
      this.shadow});
}
