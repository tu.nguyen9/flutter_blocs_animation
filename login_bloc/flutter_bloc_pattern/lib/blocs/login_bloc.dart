import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_pattern/contents/string_prefs.dart';
import 'package:flutter_bloc_pattern/events/login_event.dart';
import 'package:flutter_bloc_pattern/models/login_model/body_login.dart';
import 'package:flutter_bloc_pattern/repository/login/login_repository.dart';
import 'package:flutter_bloc_pattern/states/login_state.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';
import 'package:flutter_bloc_pattern/validators/login_valid.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> with LoginValidator {
  LoginRepository loginRepository;
  final _email = StreamController<String>.broadcast();
  final _passWord = StreamController<String>.broadcast();

  LoginBloc(LoginState initialState, this.loginRepository)
      : super(initialState);
  // Add data to Stream;
  Stream<String> get emailS => _email.stream.transform(validateEmail);
  Stream<String> get passWordS => _passWord.stream.transform(validatePassword);
  Stream<bool> get submitValid =>
      Rx.combineLatest2(emailS, passWordS, (e, p) => true);
  // Change data
  Function(String) get emailChange => _email.sink.add;
  Function(String) get passwordChange => _passWord.sink.add;
  // submitButtonLogin(){
  //   _email.stream.listen((value) {
  //     emailOk = true;
  //   });
  //   _passWord.stream.listen((event) {
  //     passWordOk = true;
  //   });
  // }

  dispose() {
    _email.close();
    _passWord.close();
  }

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    BodyLogin bodyLogin = BodyLogin();
    if (event is StartEvent) {
      yield LoginInitState();
    } else if (event is LoginButtonPress) {
      yield LoginLoadingState();
      bodyLogin.email = event.email;
      bodyLogin.password = event.password;
      // var data = await loginRepository.getLogin(
      //     SharedPreferencesUtil.getString(ApiKey), bodyLogin);
      var data = await loginRepository.getLogin(
          'U2FsdGVkX1/h5Z68zm1nmLeFG6uF+B9iP8IsXV7X/MQ=', bodyLogin);
      if (data.data.role == "User") {
        SharedPreferencesUtil.putString(Token, data.data.token);
        SharedPreferencesUtil.putString(UserId, data.data.id);
        SharedPreferencesUtil.putString(Email, data.data.email);
        yield UserLoginSuccessState();
      } else if (data.data.role == "Admin") {
        SharedPreferencesUtil.putString(Token, data.data.token);
        SharedPreferencesUtil.putString(UserId, data.data.id);
        SharedPreferencesUtil.putString(Email, data.data.email);
        yield AdminLoginSuccessState();
      }
      else{
        yield LoginErrorState(message: "Login fail");
      }
    }
  }
}

