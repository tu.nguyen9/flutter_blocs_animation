import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_pattern/contents/string_prefs.dart';
import 'package:flutter_bloc_pattern/events/app_theme_event.dart';
import 'package:flutter_bloc_pattern/states/app_theme_state.dart';
import 'package:flutter_bloc_pattern/util/app_theme_change.dart';
import 'package:flutter_bloc_pattern/util/shared_preferences_util.dart';

class AppThemeBloc extends Bloc<AppThemeEvent, AppThemeState> {

  AppThemeBloc() : super(AppThemeState(themeData: AppThemeChangeProvider.themeLightData));
  @override
  Stream<AppThemeState> mapEventToState(AppThemeEvent event) async*{
    if(event is AppThemeEvent){
      yield AppThemeState(themeData: event.themeData);
    }
  }


}
